## Overview ##

**Monovar** is a single nucleotide variant (SNV) detection and genotyping algorithm for single-cell DNA sequencing data. It takes a list of bam files as input and outputs a vcf file containing the detected SNVs.

## Installation ##

This is a fork of the original MonoVar at https://bitbucket.org/hamimzafar/monovar to make it installable via [bioconda](https://bioconda.github.io/). Please follow the instructions for [using bioconda](https://bioconda.github.io/#using-bioconda) to set up `conda` with the `bioconda` channel. Then all you have to do is:

```
conda install monovar
```

## Usage ##

The program requires multiple bam files. The bam files should be sorted by coordinates. The raw sequence reads in .fastq format should be aligned to a reference genome with the help of an aligner program (e.g., BWA ([http://bio-bwa.sourceforge.net/]())). Aligner like BWA generates sam files containing aligned reads. The sam files can be converted to compressed bam files using ```samtools view``` command (see Samtools manual for details [http://www.htslib.org/doc/samtools.html]()). 

In this fork, we have included example files from [samtools mpileup](https://github.com/samtools/samtools/tree/8eff6e8ff9f0a653abf182d824621165f204ee00/test/mpileup) to enable testing for the bioconda installation, as no successful program execution is possible without input (e.g. no `--help` or `--version` flag). But you can also run the tests without any further installation from the main folder of a clone of this fork:

```
git clone https://bitbucket.org/dlaehnemann/monovar.git
cd monovar
samtools mpileup -BQ0 -d10000 -f examples/mpileup.ref.fa -q 40 -b examples/filenames.txt | src/monovar.py -p 0.002 -a 0.2 -t 0.05 -m 2 -f examples/mpileup.ref.fa -b examples/filenames.txt -o output.vcf
```

The arguments of Monovar are as follows:

```
#!python

-b: Text file containing the full path for each Bam file. One file per line.
-f: Reference genome file.
-o: Output file.
-t: Threshold to be used for variant calling (Recommended value: 0.05)
-p: Offset for prior probability for false-positive error (Recommended value: 0.002)
-a: Offset for prior probability for allelic drop out (Default value: 0.2)
-m: Number of threads to use in multiprocessing (Default value: 1)
-c: Flag indicating whether to use Consensus Filter (CF) or not (Possible values: 0, 1; Default Value: 1; if 1 then CF is used, otherwise not used)  
```
We recommend using cutoff 40 for mapping quality when using ```samtools mpileup```. To use the probabilistic realignment for the computation of Base Alignment Quality, drop the ```-B``` while running ```samtools mpileup```.
